# Curso de Aplicaciones Móviles #

Curso impartido por Intic Labs (Daniel Olivares y Tomás Olivares)
ITESM Campus San Luis Potosí - 2 de Nov. de 2016

Aplicación: Lista "TO-DO" para llevar control de tareas pendientes y hechas



### 0. Instalar requerimientos (git, node / npm, cordova, ionic) ###
**Git**

*  [Introducción](https://www.atlassian.com/git/tutorials/what-is-version-control)

*  [Instalación](https://www.atlassian.com/git/tutorials/install-git)

*Se recomienda revisar todo el tutorial.*

**Node.js**

* [Instalación](http://nodejs.org/)

**Cordova, Ionic**

```
npm install -g cordova ionic
```

### 1. Inicializar un proyecto de Ionic ###
```
git checkout -f step-1
```
**Para este tutorial**
```
git clone https://dolivares@bitbucket.org/dolivares/todo-app.git
cd todo-app
npm install
bower install
```

**Para otros proyectos**

* [Guía](http://ionicframework.com/getting-started/)

### 2. Crear vista de tareas ###
```
git checkout -f step-2
```
### 3. Crear controlador (y funcionalidad) de tareas ###
```
git checkout -f step-3
```
### 4. Esconder tareas listas ###
```
git checkout -f step-4
```
### 5. Agregar tareas ###
```
git checkout -f step-5
```
### 6. Persistir tareas ###
```
git checkout -f step-6
```

### 7. Eliminar tareas ###
```
git checkout -f step-7
```