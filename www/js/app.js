angular.module("todo-app", ["ionic", "ngStorage"])
.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
  
  // Adds "/tasks" route. This will show the specified template and use
  // the specified controller.
  $stateProvider
  .state("tasks", {
    url: "/tasks",
    templateUrl: "templates/tasks.html",
    controller: "TasksCtrl"
  });
  
  // If no routes found, go to the default one ("/tasks")
  $urlRouterProvider.otherwise("/tasks");
}]);