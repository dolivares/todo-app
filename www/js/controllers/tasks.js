angular.module("todo-app")
.controller("TasksCtrl", ["$scope", "$localStorage", function ($scope, $localStorage) {
  $scope.storage = $localStorage;
  // If $scope.storage.tasks is undefined, initialize it to an empty array,
  // otherwise set it equal to existent tasks.
  $scope.tasks = $scope.storage.tasks == undefined ? [] : $scope.storage.tasks;
  
  // Don't allow reordering items initially
  $scope.options = {
    showReorder: false,
    showDone: true,
    showDelete: false
  };
  
  // Switches $scope.options.showReorder
  $scope.toggleShowReorder = function () {
    $scope.options.showReorder = !$scope.options.showReorder;
    
    // Only one from reorder and deletion works at a time.
    if ($scope.options.showReorder) $scope.options.showDelete = false;
  }
  
  // Switches $scope.options.showDelete
  $scope.toggleShowDelete = function () {
    $scope.options.showDelete = !$scope.options.showDelete;
    
    // Only one from reorder and deletion works at a time.
    if ($scope.options.showDelete) $scope.options.showReorder = false;
  }
  
  // Move task after reordering it.
  $scope.moveTask = function(task, fromIndex, toIndex) {
    $scope.tasks.splice(fromIndex, 1); // deletes task from array
    $scope.tasks.splice(toIndex, 0, task); // adds task to toIndex
  };
  
  // Adds a new task with the given text
  $scope.addTask = function () {
    // Push a new task to the tasks array
    $scope.tasks.push({
      text: $scope.newTask,
      done: false
    });
    
    // Make sure to persist tasks locally
    $scope.storage.tasks = $scope.tasks;
    
    // Reset newTask after adding it
    $scope.newTask = "";
  }
  
  // Deletes a task from the list
  $scope.deleteTask = function (index) {
    $scope.tasks.splice(index, 1); // deletes it from array
    
    // Make sure to persist tasks locally
    $scope.storage.tasks = $scope.tasks;
  }
}]);